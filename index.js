const express = require('express');
const app = express();

app.get('/', (req, res) => {
   res.send('Hello Sohel Rana. Welcome to Ergo Ventures. Today is 5th Match 2021')
});

app.listen(8080, () => {
    console.log("Listening on port 8080");
})